#!/bin/bash
#
# This file is produced as a part of an educational course.
#
# ... DESCRIBE PURPOSE OF FILE HERE ...
#
# Copyright (C) 2021, Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Copyright (C) 2021, Your Name <your@email.address>
#
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This file is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with Gnuastro. If not, see <http://www.gnu.org/licenses/>.
