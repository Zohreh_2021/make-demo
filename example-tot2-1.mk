# This make file convert a txt file to a fits file
#
# Copyright (C) 2021, Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Copyright (C) 2021, Zohreh Ghaffari <zghaffari@iac.es>
#
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This file is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with Gnuastro. If not, see <http://www.gnu.org/licenses/>.





# User setting
BDIR=/home/zohreh/make-demo





# Make settings
# Default Goal
all: check.txt

# We define our shell path
SHELL: /usr/bin/bash

# Error alarm
.SHELLFLAGS = -ec

# United all shell commands
.ONESHELL:





# Create a matrix 3x3
list=$(BDIR)/a.txt
$(list):
	@echo "1 1 1" >  $@
	echo "1 2 2" >>  $@
	echo "1 1 1" >>  $@





# Convert the matrix to a fits file
#printf "%-10s %-10s %-10s\n%-10.3g %-10.3g %-10.3g \n" mean median stdprintf "%-10s %-10s %-10s\n%-10.3g %-10.3g %-10.3g \n" mean median std
a.fits: $(list)
	astconvertt $< --output=$@





# Sanity check
check.txt: a.fits
	echo "mean=     median=      std="
	aststatistics $< --mean --median --std \
	              > $@ 
	cat $@




# Clean the files
clean:
	rm -f a.fits a.txt check.txt
