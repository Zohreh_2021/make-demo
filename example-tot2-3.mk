# This make-file will download hst images.
# Copyright (C) 2022, Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Copyright (C) 2022, Zohreh Ghaffari <zghaffari@iac.es>
#
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This file is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with Gnuastro. If not, see <http://www.gnu.org/licenses/>.





# Make settings
# Default Goal (the gholabi hadaf)
all:final

# Run all the commands i n the rcipe in on shell
.ONESHELL:

# Shell to use in the recipe
SHELL=/usr/local/bin/bash

# Stops the recipe's shell if the commands fail
.SHELLFLAGS = -ec

# Import using include
include config-pipline/*.conf




# Create the high level directory structure
ncdir=$(BDIR)/nc
segdir=$(BDIR)/seg
catdir=$(BDIR)/cat
flatdir=$(BDIR)/flat
downdir=$(BDIR)/download
$(BDIR):; mkdir $@ && ln -sf $(BDIR) .build
$(ncdir) $(segdir) $(catdir) $(flatdir) $(downdir): | $(BDIR); mkdir $@





# Download the raw images
raw=$(foreach f, $(FILTERS),$(downdir)/$(xdfprefix)$(f)$(xdfsuffix))
# In this senario the filenames are same and only path are different
$(raw): $(downdir)/%: | $(downdir)
#	full=$(xdfurl)/$(notdir $@)
#	wget -O$@ $$full
	wget -O$@ $(xdfurl)/$*
#	exit 1




# Flat
flat=$(foreach f, $(FILTERS),$(flatdir)/$(f).fits)
# In this senario both filenames are different
# % is only the name of filter
# HST files are stored in h0
$(flat): $(flatdir)/%.fits: $(downdir)/$(xdfprefix)%$(xdfsuffix) \
          $(vertices.conf) | $(flatdir)
	deep_polygon=$(vertice1):$(vertice2):$(vertice3):$(vertice4)
	astcrop $< --mode=wcs -h0 --polygon=$$deep_polygon --output=$@




# Create a gaussian kernel
kernel=$(ncdir)/kernel.fits
$(kernel): | $(ncdir)
	astmkprof --kernel=gaussian,1.5,5 --oversample=1 \
	          --output=$@





# Detection of all sources in all input files 
nc-conf=config-prog/nc.conf
nc=$(foreach f, $(FILTERS),$(ncdir)/$(f).fits)
$(nc): $(ncdir)/%.fits: $(flatdir)/%.fits $(kernel) $(nc-conf) | $(ncdir)
	astnoisechisel $< --kernel=$(kernel) \
	               --config=$(nc-conf) --output=$@




# Separate all the clumps of the sources
seg=$(foreach f,$(FILTERS),$(segdir)/$(f).fits)
$(seg): $(segdir)/%.fits: $(ncdir)/%.fits | $(segdir)
	astsegment $< --output=$@




# Catalog of all filters respect to the reference filter
cat=$(foreach f,$(FILTERS),$(catdir)/$(f).fits)
$(cat): $(catdir)/%.fits: $(segdir)/%.fits $(segdir)/$(REFCATFILT).fits | $(catdir) 
#	I will use the % or equivalent $* to have the proper zeropoint
#	for each filter
	if [ $* = f105w ]; then
	  zeroval=26.27
	elif [ $* = f125w ]; then
	  zeroval=26.23
	elif [ $* = f160w ]; then
	  zeroval=25.94
	else
	  echo "ERROR: '$*' not recognized"; exit 2
	fi
	astmkcatalog $(segdir)/$(REFCATFILT).fits --ids --ra \
	             --dec --magnitude --sn --zeropoint=$$zeroval \
	             --clumpscat --valuesfile=$< --output=$@




# To merged the three cataloge and create colors
merged-catalog=$(catdir)/three-in-one.fits)
refcat=$(catdir)/$(reffilt).fits

$(merged-catalog): $(cat)
	@echo "check: $@ "

	asttable $(refcat) -hCLUMPS --output=$@ \
	         --catcolumnfile=$(catdir)/f125w.fits \
	         --catcolumnfile=$(catdir)/f105w.fits \
	         --catcolumnhdu=CLUMPS --catcolumnhdu=CLUMPS \
	         --catcolumns=MAGNITUDE




final: $(cat)



