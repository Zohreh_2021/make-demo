# This file is part of my exercise to learn make
#
# ... I want to make a dir if it doesn't exist before ...
#
# Copyright (C) 2021, Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Copyright (C) 2021, Your Name <zoh.ghaffari@email.address>
#
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This file is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with Gnuastro. If not, see <http://www.gnu.org/licenses/>.

# Variables can represent lists of file names, options to pass to
# compiler, programs to run, directories to look in for source files,
# directories to write output in, or anything else you can imagine

# Automatic variables:
# $@ ~ target, $< ~first prerequisites, $^ ~ all prerequisites ( uniq)
# , $+ all prequisit, $| ~ all order-only prerequisites, $* the stem
# with which an implicit rule (%) matches. dir/a.foo.b with the rule
# 'a.%.b' => 'dir/foo', the stem is useful for constructing names of
# related file.  In a static pattern rule, the stem is part of the
# filename that matches the '%' in the TARGET PATTERN
#$? ~ changed prerequisites




# $(@D) The dir part of the filename with slash removed dir/foo.fits
# -> 'dir' $(@F) The file name within the directory part of the
# filename 'dir/foo.fits' -> 'foo.fits'
# $(@F) ~ $(notdir $@)




# Pattern rule:
A pattern rule contains the chracter '%' in the target.
'%' matches any nonempty substring, while other characters match only themselves.


# Basic dir
bdir=/home/zghaffari/make-demo


# Default Goal
drk-dir=$(bdir)/mdark

# to ways for writing reciep in one line or too use tab in the second
#line
#$(drk-dir):; mkdir $@
$(drk-dir):
	mkdir $@



# Create output dir 
outdir=/home/make-demo/mdark
drk-outdir=$(bdir)/dark
$(drk-outdir): | $(dark-dir) ; mkdir $@





