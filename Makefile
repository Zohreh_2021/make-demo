
# This file is produced as a part of an educational course.
#
# ... DESCRIBE PURPOSE OF FILE HERE ...
#
# Copyright (C) 2021, Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Copyright (C) 2021, Your Name <your@email.address>
#
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This file is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with Gnuastro. If not, see <http://www.gnu.org/licenses/>.

# Default goal
all: sim.fits
# Clean
clean:
	rm -f *.fits

# Run MakeProfile to create an oversampled FITS image.
# Convolve the created image with the kernel.
conv.fits: cat.txt mk.conf conv.conf
	a=3
	astmkprof cat.txt --config=mk.conf
	@echo 
	astconvolve --config=conv.conf cat_profiles.fits -o$@
	exit 1
# Scale the image back to the intended resolution
warp.fits: conv.fits wp.conf
	@echo
	astwarp conv.fits --config=wp.conf -o$@

# Crop the edges out
crop.fits: warp.fits cr.conf
	@echo
	astcrop warp.fits --config=cr.conf -o$@

# Add noise to the image
sim.fits: crop.fits ns.conf
	@echo
	astmknoise crop.fits --config=ns.conf -o$@

