#all:
#	@echo no\
space
#	@echo no\
#	space
#	@echo one \
#	space
#	@echo one\
#	 space
#all: ; @echo 'hello \
#	world' ; echo "hello \
#    world"	
#TXT = 'hello \
#world'
#all: ; @echo $(TXT)
.ONESHELL:
LIST= one two three
all:
	for i in $(LIST); do \
	    echo $$i; \
	done
	@echo
	@echo About to make distribution file
ifeq (0, ${MAKELEVEL})
whoami    := $(shell whoami)
host-type := $(shell arch)
MAKE      := ${MAKE} host-type=${host-type} whoami=${whoami}
endif
clean:
	rm *.fits
