# This make-file will download hst images.
# Copyright (C) 2021, Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Copyright (C) 2021, Zohreh Ghaffari <zghaffari@iac.es>
#
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This file is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with Gnuastro. If not, see <http://www.gnu.org/licenses/>.





# User setting
BDIR=/scratch/build/make-demo/tutorial2-2
FILTERS=f105w f125w f160w



# Make settings
# Default Goal (the gholabi hadaf)
all:final




# Run all the commands i n the rcipe in on shell
.ONESHELL:

# Shell to use in the recipe
SHELL=/usr/local/bin/bash

# Stops the recipe's shell if the commands fail
.SHELLFLAGS = -ec


# Import using include
include config/*.conf


# Create the high level directory structure
flatdir=$(BDIR)/flat
downdir=$(BDIR)/download
$(BDIR):; mkdir $@ && ln -sf $(BDIR) .build
$(flatdir) $(downdir): | $(BDIR); mkdir $@




# Download image
raw=$(xdfprefix)$(FILTERS)$(xdfsuffix)
image=$(downdir)/$(raw)
$(image): | $(downdir)
	full=$(xdfurl)/$(raw)
	@echo "Check target: $(image)"
	echo "Check raw: $(raw)"
	echo "Check full: $$full"
	wget -O$@ $$full




# To work on flat part
flat=$(flatdir)/xdf-$(FILTERS).fits
$(flat): $(vertices.conf) $(image) | $(flatdir)
	deep_polygon=$(vertice1):$(vertice2):$(vertice3):$(vertice4)
	astcrop $(image) --mode=wcs -h0 \
	        --polygon=$$deep_polygon --output=$@




#To not be worry about variables
# The variables are close to the rule of that step
final:$(flat)




# Clean the files
clean:
	rm -r $(flat)
